SPECFILE            = $(shell find -maxdepth 1 -type f -name *.spec)
SPECFILE_NAME       = $(shell awk '$$1 == "Name:"     { print $$2 }' $(SPECFILE) )
SPECFILE_VERSION    = $(shell awk '$$1 == "Version:"  { print $$2 }' $(SPECFILE) )
SPECFILE_RELEASE    = $(shell awk '$$1 == "Release:"  { print $$2 }' $(SPECFILE) )
TARFILE             = $(SPECFILE_NAME)-$(SPECFILE_VERSION).tgz
DIST               ?= $(shell rpm --eval %{dist})

.PHONY: sources download check clean tag rpm srpm

#
# download and check the files declared in the "sources" file
#

sources: download check

download:
	@cat sources | while read line; do \
	    set $$line; \
	    curl -L -O -R -S -f -s $$2 ; \
	done

check:
	@cat sources | while read line; do \
	    set $$line; \
	    echo "$$1  `basename $$2`" | md5sum -c; \
	done

clean:
	@cat sources | while read line; do \
	    set $$line; \
	    rm -fv `basename $$2`; \
	done
	rm -rf build/

tag:
	@tag=`perl -ne 'print("v$$2\n") if m{/(apache-activemq|patch)-(.+?)(-bin)?(\.\w+)+$$}' sources | tail -n 1`; \
	seen=`git tag -l | grep -Fx $$tag`; \
	if [ "x$$seen" = "x" ]; then \
	    set -x; \
	    git tag $$tag; \
	    git push --tags; \
	else \
	    echo "already tagged with $$tag"; \
	fi

rpm: sources
	rpmbuild -bb --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)
