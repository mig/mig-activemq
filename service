#!/bin/sh
################################################################################
#                                                                              #
# Simplified version of the ActiveMQ init script that only works on Linux but  #
# that can understand multiple independent broker instances (different JVMs).  #
#                                                                              #
# Note: the exit codes are LSB-compliant, see http://goo.gl/vQqaC              #
#                                                                              #
################################################################################
#                                                                              #
# Copyright (C) CERN 2012-2025                                                 #
#                                                                              #
#   Licensed under the Apache License, Version 2.0 (the "License");            #
#   you may not use this file except in compliance with the License.           #
#   You may obtain a copy of the License at                                    #
#                                                                              #
#       http://www.apache.org/licenses/LICENSE-2.0                             #
#                                                                              #
#   Unless required by applicable law or agreed to in writing, software        #
#   distributed under the License is distributed on an "AS IS" BASIS,          #
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
#   See the License for the specific language governing permissions and        #
#   limitations under the License.                                             #
#                                                                              #
################################################################################

# check the mandatory variables
for _var in ACTIVEMQ_BASE ACTIVEMQ_HOME APP_NAME WRAPPER_CMD WRAPPER_CONF; do
    [ -n "${!_var}" ] && continue
    echo "Error: ${_var} is not set"
    exit 1
done

# set the other variables
lockfile="${LOCKFILE:-/var/lock/subsys/${APP_NAME}}"
pidfile="${PIDDIR:-${ACTIVEMQ_BASE}/tmp}/${APP_NAME}.pid"
service=${APP_LONG_NAME:-${APP_NAME}}

# really start the daemon (quotes are needed in case some paths contain spaces)
real_start() {
    [ -d "${ACTIVEMQ_BASE}/tmp" ] && rm -fr "${ACTIVEMQ_BASE}/tmp/"jetty-*
    _cmd="\"${WRAPPER_CMD}\" \"${WRAPPER_CONF}\" \"wrapper.syslog.ident=${APP_NAME}\" \
          \"wrapper.pidfile=${pidfile}\" wrapper.daemonize=TRUE"
    if [ -z "${RUN_AS_USER}" -o `id -un` = "${RUN_AS_USER}" ]; then
	eval ${_cmd}
    else
	runuser -s /bin/sh ${RUN_AS_USER} -c "${_cmd}"
    fi
}

# really stop the daemon
real_stop() {
    _pid=`cat "${pidfile}"`
    [ -n "${_pid}" -a -d "/proc/${_pid}" ] && kill $@ ${_pid} >/dev/null
}

# check the status (0 = ok, 3 = stopped, 1 = dead with pidfile)
# (as a side effect, also update the lock file and remove dead pid file)
lsb_status() {
    if [ -f "${pidfile}" ]; then
        _pid=`cat "${pidfile}"`
        if [ -d "/proc/${_pid}" ]; then
            touch "${lockfile}"
            return 0
        else
            rm -f "${lockfile}" "${pidfile}"
            return 1
        fi
    else
        rm -f "${lockfile}"
        return 3
    fi
}

# stop the daemon (0 = not running or successfully stopped, 1 = failed to stop)
lsb_stop() {
    lsb_status
    [ $? -eq 0 ] || return 0
    real_stop $@
    for _i in {1..9}; do
	sleep 1
        lsb_status
        [ $? -eq 0 ] || return 0
    done
    return 1
}

# start the daemon (0 = already running or successfully started, 1 = failed to start)
lsb_start() {
    lsb_status
    [ $? -eq 0 ] && return 0
    real_start
    for _i in {1..9}; do
        sleep 1
        lsb_status
        [ $? -eq 0 ] && return 0
    done
    return 1
}

# main
case "$1" in
    start)
	echo "Starting ${service}..."
        lsb_start
	retval=$?
        ;;
    stop)
	echo "Gracefully stopping ${service}..."
        lsb_stop
	retval=$?
        ;;
    force-stop)
	echo "Forcibly stopping ${service}..."
        lsb_stop -9
	retval=$?
        ;;
    restart|force-reload)
	echo "Restarting ${service}..."
        lsb_stop && lsb_start
        retval=$?
        ;;
    status)
        lsb_status
        retval=$?
	if [ ${retval} -eq 0 ]; then
	    _pid=`cat "${pidfile}"`
	    echo "${service} is running (pid=${_pid})"
	else
	    echo "${service} is not running"
	fi
        ;;
    *)
        echo "Usage: $0 {start|stop|force-stop|restart|force-reload|status}"
        retval=2
	;;
esac
#echo "Done (retval=${retval})"
exit ${retval}
