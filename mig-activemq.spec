%define debug_package %{nil}
%define __jar_repack %{nil}
%define _build_id_links none
%define source_url() %((grep %1 %{_sourcedir}/sources || \
                        grep %1  %{_builddir}/sources || \
  echo "x unknown") 2>/dev/null | head -n 1 | perl -ane 'print("$F[1]")')

# find wrapper information
%define wrapper_url %source_url /wrapper-linux-
%define wrapper_tar %(basename %{wrapper_url})
%define wrapper_version %(echo %{wrapper_tar} | \
  perl -pe 's/.+-//; s/\\.tar\\.gz$//')

# find ActiveMQ information
%define activemq_url %source_url /apache-activemq-
%define activemq_tar %(basename %{activemq_url})
%define activemq_version %(echo %{activemq_tar} | \
  perl -pe 's/apache-activemq-//; s/-bin\\.(tar\\.gz|zip)$//')
%define is_snapshot %(echo %{activemq_version} | \
  perl -ne 'print /-\\d{8}\\.\\d{6}-\\d+$/ ? 1 : 0')

# deduce rpm and tar information
%if %{is_snapshot}
  %define rpm_release %(echo %{activemq_version} | \
    perl -pe 's/.+-(\\d{8}\\.\\d{6})-(\\d+)$/0.$1.$2/')
  %define maj_version %(echo %{activemq_version} | \
    perl -pe 's/-\\d{8}\\.\\d{6}-\\d+$//')
  %define tar_version %{maj_version}-SNAPSHOT
  %define rpm_unclean_version %{maj_version}
%else
  %define rpm_release 1
  %define tar_version %{activemq_version}
  %define rpm_unclean_version %{activemq_version}
%endif
%define rpm_version %(echo %{rpm_unclean_version} | perl -pe 's/-/_/g')

# installation settings (main)
%define activemq_dname /usr/share
%define activemq_sname activemq
%define activemq_vname activemq-%{activemq_version}

# installation settings (client)
%define alljar_dname %{_javadir}
%define alljar_sname activemq-all.jar
%define alljar_vname activemq-all-%{activemq_version}.jar

Name:          mig-activemq
Version:       %{rpm_version}
Release:       %{rpm_release}%{?dist}
Summary:       Apache ActiveMQ
Group:         System
License:       Apache-2.0
Url:           https://activemq.apache.org/
Source0:       %{activemq_tar}
Source1:       sources
Source2:       service
Source3:       wrapper-linux-x86-64-%{wrapper_version}.tar.gz
Source4:       wrapper-linux-arm-64-%{wrapper_version}.tar.gz
BuildRoot:     %{_tmppath}/%{name}-%{pkgversion}-%{release}-root
BuildRequires: perl

%description
Apache ActiveMQ Classic is a popular and powerful open source messaging
and Integration Patterns server.

It supports many Cross Language Clients and Protocols, comes with easy
to use Enterprise Integration Patterns and many advanced features while
supporting Jakarta Messaging 3.1, JMS 2.0, and JMS 1.1 and J2EE 1.4+.

This package includes Tanuki Software's Java Service Wrapper Community
Edition version %{wrapper_version}.

%package client
Summary:       Apache ActiveMQ client jar
Group:         System
Provides:      activemq-all.jar

%description client
Apache ActiveMQ's Java client library (activemq-all.jar).

%prep
%ifarch x86_64
%setup -q -n wrapper -c -T -a 3
%endif
%ifarch aarch64
%setup -q -n wrapper -c -T -a 4
%endif
%setup -q -n apache-activemq-%{tar_version}

%build
###
pwd
ls -l ..
find ../wrapper
###
# copy the service script to the bin directory
install -m 0755 %{SOURCE2} bin/service
# copy the wrapper files to the bin directory
install -m 0755 ../wrapper/*/bin/wrapper       bin/wrapper
install -m 0755 ../wrapper/*/lib/libwrapper.so bin/libwrapper.so
install -m 0644 ../wrapper/*/lib/wrapper.jar   bin/wrapper.jar
# remove the (now obsolete) wrapper architecture directories
rm -fr bin/linux* bin/macosx bin/win32 bin/win64
# remove the useless Windows scripts
rm -f bin/*.bat
# make the Ruby examples non-executable to avoid adding a spurious dependency
chmod -x `find example* -type f -name '*.rb'`
# fix the pom.xml files permissions
chmod -x `find example* -type f -name pom.xml`
# hard-link favicon.ico so that it is visible from the admin interface
ln webapps/favicon.ico webapps/admin/favicon.ico

%install
rm -fr %{buildroot}
# move the activemq-all jar away (it will be part of the client rpm)
mkdir -p %{buildroot}%{alljar_dname}
mv activemq-all-*.jar %{buildroot}%{alljar_dname}/%{alljar_vname}
# everything else goes into the main rpm
mkdir -p %{buildroot}%{activemq_dname}/%{activemq_vname}
mv * %{buildroot}%{activemq_dname}/%{activemq_vname}

%clean
rm -fr %{buildroot}

%post
cd %{activemq_dname}
rm -f %{activemq_sname}
ln -s %{activemq_vname} %{activemq_sname}

%post client
cd %{alljar_dname}
rm -f %{alljar_sname}
ln -s %{alljar_vname} %{alljar_sname}

%postun
cd %{activemq_dname}
rm -f %{activemq_sname}
last=`ls -dt activemq-* 2>/dev/null | head -1`
[ "x${last}" != "x" ] && ln -s ${last} %{activemq_sname} || true

%postun client
cd %{alljar_dname}
rm -f %{alljar_sname}
last=`ls -dt activemq-all-*.jar 2>/dev/null | head -1`
[ "x${last}" != "x" ] && ln -s ${last} %{alljar_sname} || true

%files
%defattr(-,root,root,-)
%{activemq_dname}/%{activemq_vname}

%files client
%defattr(-,root,root,-)
%{alljar_dname}/%{alljar_vname}
